from netpbmfile import *
import numpy as np
import demosaic as dm

def raw2ppm(inname, outname):
	#maxval = 2**8-1;
	maxval = 2**12-1;
	image = imread(inname, maxval=maxval)

	image = image / float(maxval) # numpy doesn't detect input bit depth


	image = (dm.demosaic(image, pattern='grbg',method='hq_linear')*maxval).astype(np.uint16)
	imsave(outname, image, maxval=maxval)