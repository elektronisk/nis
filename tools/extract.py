import os, sys
from datetime import datetime
from struct import *
from array import array
import numpy as np
from netpbmfile import imsave
from raw2ppm import raw2ppm

if len(sys.argv) < 2:
	print 'Not enough arguments. Specify log file name.'
	sys.exit()

if not os.path.isfile(sys.argv[1]):
	print 'Log file not found'
	sys.exit()

logfilename = sys.argv[1]

fdata = open(logfilename, "rb").read()

start = fdata.find('####')
end = fdata.find('****')
if start == -1 or end == -1:
    print 'Could not find start or end marker for size'
    sys.exit()

start = start+4 # skip leading ####
parameters = fdata[start:end].split(':') # split parameters separated by :
if len(parameters) != 3:
	print 'Not 3 parameters: ' + str(parameters)
	sys.exit()
address = parameters[0].lower()
sizestr = parameters[1].lower()
skipstr = parameters[2].lower()
size = int(sizestr, 16)
skip = int(skipstr, 16)
start = end+4
end = start + size

data = fdata[start:end]
filename = datetime.now().strftime('%Y-%m-%dT%H-%M-%S.%f') + ' '+ address +'-' + sizestr + '.dat'

udata = array("H")
udata.fromstring(data)
npdata = np.array(udata, dtype=np.uint16)
#np.set_printoptions(edgeitems = 5)
print npdata.shape
npdata = npdata.reshape((1944/skip,-1))
print npdata.shape
print npdata

imsave(filename + '.pgm', npdata, maxval=2**12 -1)
npdata.tofile(open(filename, 'wb'))
raw2ppm(filename + '.pgm', filename + '.ppm')