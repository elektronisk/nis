#include <QtSerialPort/QSerialPort>
#include <QTextStream>
#include <QCoreApplication>
#include <QStringList>
#include <QFile>

QT_USE_NAMESPACE

#define PROGRESS_DOT_INTERVAL 16*1024
#define PROGRESS_TEXT_INTERVAL PROGRESS_DOT_INTERVAL*64

int main(int argc, char *argv[])
{
	QCoreApplication coreApplication(argc, argv);
	int argumentCount = QCoreApplication::arguments().size();
	QStringList argumentList = QCoreApplication::arguments();

	QTextStream standardout(stdout);

	if (argumentCount != 5) {
		standardout << QString("Usage: %1 <serialportname> <baudrate> <scriptfile> <logfilename>").arg(argumentList.first()) << endl;
		return 1;
	}

	QSerialPort serialPort;
	QString serialPortName = argumentList.at(1);
	serialPort.setPortName(serialPortName);

	int serialPortBaudRate = argumentList.at(2).toInt();
	serialPort.setBaudRate(serialPortBaudRate);

	// open files used for input and output, as well as the serial port
	QString scriptFileName = argumentList.at(3);
	QFile scriptFile(scriptFileName);
	if (!scriptFile.open(QIODevice::ReadOnly)) {
		standardout << QString("Failed to open script file for reading: %1").arg(scriptFile.errorString()) << endl;
		return 1;
	}
	QByteArray script = scriptFile.readAll();

	QString logFileName = argumentList.at(4);
	QFile logFile(logFileName);
	if (!logFile.open(QIODevice::WriteOnly)) {
		standardout << QString("Failed to open log file for writing: %1").arg(logFile.errorString()) << endl;
		return 1;
	}
	QDataStream logStream(&logFile);

	if (!serialPort.open(QIODevice::ReadWrite)) {
		standardout << QString("Failed to open port %1: %2").arg(serialPortName).arg(serialPort.errorString()) << endl;
		return 1;
	}

	// send the input data, then log all output to file
	standardout << "Executing script:" << endl << script << endl;
	serialPort.write(script);

	QByteArray readData = serialPort.readAll();
	int lastProgressDot = 0;
	int lastProgressText = 0;
	while (serialPort.waitForReadyRead(250)) {
		readData.append(serialPort.readAll());
		int nowsize = readData.size();
		if (nowsize > lastProgressDot + PROGRESS_DOT_INTERVAL) {
			standardout << '.' << flush;
			lastProgressDot += PROGRESS_DOT_INTERVAL;
		}
		if (nowsize > lastProgressText + PROGRESS_TEXT_INTERVAL) {
			standardout << " " << lastProgressDot/1024 << "K" << endl << flush;
			lastProgressText += PROGRESS_TEXT_INTERVAL;
		}
	}

	standardout << endl;

	if (serialPort.error() == QSerialPort::ReadError) {
		standardout << QString("Failed to read from port %1: %2").arg(serialPortName).arg(serialPort.errorString()) << endl;
		return 1;
	} else if (serialPort.error() == QSerialPort::TimeoutError && readData.isEmpty()) {
		standardout << QString("No data was received").arg(serialPortName) << endl;
		return 1;
	}

	standardout << QString("Total: %1 bytes received and saved to %2.").arg(readData.size()).arg(logFileName) << endl;

	logStream.writeBytes(readData.constData(), readData.size());

	standardout << '\a'; // ring system bell

	return 0;
}
