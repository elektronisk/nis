################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/cli/commands/dump.c \
../src/cli/commands/gpio.c \
../src/cli/commands/img.c \
../src/cli/commands/mem.c \
../src/cli/commands/util.c \
../src/cli/commands/vdma.c 

OBJS += \
./src/cli/commands/dump.o \
./src/cli/commands/gpio.o \
./src/cli/commands/img.o \
./src/cli/commands/mem.o \
./src/cli/commands/util.o \
./src/cli/commands/vdma.o 

C_DEPS += \
./src/cli/commands/dump.d \
./src/cli/commands/gpio.d \
./src/cli/commands/img.d \
./src/cli/commands/mem.d \
./src/cli/commands/util.d \
./src/cli/commands/vdma.d 


# Each subdirectory must supply rules for building sources it contributes
src/cli/commands/%.o: ../src/cli/commands/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MicroBlaze gcc compiler'
	mb-gcc -Wall -O0 -g3 -c -fmessage-length=0 -Wl,--no-relax -I../../camera_app_bsp/microblaze_0/include -mlittle-endian -mxl-barrel-shift -mxl-pattern-compare -mcpu=v8.50.a -mno-xl-soft-mul -mhard-float -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


