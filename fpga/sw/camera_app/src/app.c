#include <stdio.h>

#include "platform.h"
#include "xparameters.h"
#include "xiic.h"
#include "cli/cli.h"
#include "cli/commands/img.h"
#include "ee_printf.h"
void print(char *str);

XIic iic_instance;

int main() {
	int ret = 0;
	ret = XIic_Initialize(&iic_instance, 0);
	print("Initializing I2C.. ");
	if (ret != XST_SUCCESS) {
		ee_printf("Init error %i\n");
	} else {
		print("OK\n");
	}
	XIic_SetGpOutput(&iic_instance, 0x80); // enable extclk to sensor
	img_init(); // configure sensor clock sensitivity, other parameters
	print("Application running!\n");

	cli_loop();
	print("End of program\n");
	return 0;
}
