#ifndef EE_PRINTF_H_
#define EE_PRINTF_H_

#define HAS_FLOAT

int ee_printf(const char *fmt, ...);

#endif /* EE_PRINTF_H_ */
