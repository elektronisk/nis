#ifndef CLI_H_
#define CLI_H_
#include "xil_types.h"

#define LINE_BUFFER_COUNT 5
#define LINE_BUFFER_SIZE 32

typedef void(*shell_function_t)(char *args);

typedef struct cli_command_definition_t{
	const char* command_name;
	const char* const help_string;
	const shell_function_t command;
} cli_command_definition_t;

struct line_buffer { // single line
	char line[LINE_BUFFER_SIZE];
	u8 length;
	u8 cursor;
};

struct line_buffer_history {
	struct line_buffer lines[LINE_BUFFER_COUNT];
	struct line_buffer *current_line;
	u8 current;
};

char* get_parameter(char* command_string, u8 wanted_parameter, u8* parameter_length);
void cli_loop();

#endif /* CLI_H_ */
