#include "../platform.h"
#include "xparameters.h"
#include "xil_types.h"
#include "xuartlite_l.h"
#include <stdbool.h>
#include <string.h>
#include "cli.h"
#include <ctype.h>
#include "commands/util.h"
#include "commands/vdma.h"
#include "commands/mem.h"
#include "commands/img.h"
#include "commands/gpio.h"
#include "commands/dump.h"
#include "commands/control.h"
#include "../ee_printf.h"

static void print_help(char *args);

static struct line_buffer_history history;

enum Direction {Right, Left, Up, Down};
static cli_command_definition_t cmd_table[] = {
	{"help", "Prints this help", &print_help},
	{"r","Reboots", &util_reboot},
	{"vdma","Initialize VDMA to capture a frame", &vdma},
	{"mem", "Read or write memory content", &util_mem},
	{"key", "Print entered keycodes", &util_printkey},
	{"img", "Read/write image sensor registers", &img},
	{"gpio", "Set GPIO state", &gpio},
	{"dump", "Dump image contents or histogram", &dump},
	{"ctrl","Control the camera",&control}
};

/* Clears the entire history by resetting the buffers and setting current line to first line*/
void history_reset(struct line_buffer_history *hist) {
	memset(hist, 0, sizeof(struct line_buffer_history));
	hist->current_line = &hist->lines[0];
}

void line_buffer_reset(struct line_buffer *buf) {
	memset(buf, 0, sizeof(struct line_buffer));
}

/* Inserts a given character into given line buffer, at its cursor location. */
void line_buffer_insert_key(struct line_buffer *buf, u8 c) {
	char *p;
	if(isprint(c) && buf->length < LINE_BUFFER_SIZE -2) {
		for (p = &(buf->line[buf->length]); p > &(buf->line[buf->cursor]); p--) { // shift characters in front of cursor to the right
			*p = *(p-1);
		}
		buf->line[buf->cursor] = c;
		buf->cursor++;
		buf->length++;
		buf->line[buf->length] = '\0';
	}
}
inline void history_insert_key(struct line_buffer_history* hist, u8 c) {
	line_buffer_insert_key(hist->current_line, c);
}

/* Removes the key to the left of cursor (dir=Left), or under cursor (dir=Right), like backspace and delete*/
void line_buffer_remove_key(struct line_buffer *buf, enum Direction dir) {
	char *p;
	if (dir == Left) {
		if (buf->cursor > 0) {
			for (p = &(buf->line[buf->cursor]); p < &buf->line[buf->length]; p++) {
				*(p-1) = *p;
			}
			buf->length--;
			buf->cursor--;
			buf->line[buf->length] = '\0';
		}
	} else {
		if (buf->cursor < buf->length) {
			for (p = &(buf->line[buf->cursor]); p < &buf->line[buf->length]; p++) {
				*p = *(p+1);
			}
			buf->length--; // cursor remains in same place
			buf->line[buf->length] = '\0';
		}
	}

}
inline void history_remove_key(struct line_buffer_history *hist, enum Direction dir) {
	line_buffer_remove_key(hist->current_line, dir);
}

void history_move_cursor(struct line_buffer_history* hist, enum Direction dir) {
	switch(dir) {
		case Right:
			if (hist->current_line->cursor < hist->current_line->length)
				hist->current_line->cursor++;
			break;
		case Left:
			if (hist->current_line->cursor > 0)
				hist->current_line->cursor--;
			break;
		case Up:
			hist->current = (hist->current == 0) ? (LINE_BUFFER_COUNT-1) : (hist->current - 1);
			hist->current_line = &hist->lines[hist->current];
			break;
		case Down:
			hist->current = (hist->current + 1) % LINE_BUFFER_COUNT;
			hist->current_line = &hist->lines[hist->current];
			break;
		default:
			break;
	}
}

/* The following function is from the NUTS software repository */
char *get_parameter(char *command_string, u8 wanted_parameter, u8 *parameter_length){
	u8 parameter_number = 0;
	u8 ret_parameter_length = 0;

	//Check for input errors
	if(command_string == NULL || parameter_length == NULL){
		return NULL;
	}

	//Find first non-whitespace
	while(isspace((int)*command_string)) command_string++;

	//Find wanted parameter
	while(parameter_number != wanted_parameter){
		command_string = strpbrk(command_string, " ");
		if(command_string == NULL){
			return NULL;
		}
		while(isspace((int)*command_string)) command_string++;
		if(*command_string == '\0'){
			return NULL;
		}
		parameter_number++;
	}

	//Find length of parameter
	for(ret_parameter_length = 0; command_string[ret_parameter_length] != '\0' && (!isspace((int)command_string[ret_parameter_length])); ret_parameter_length++);

	*parameter_length = ret_parameter_length;
	return ret_parameter_length == 0 ? NULL : command_string;
}

/* The following function is from the NUTS software repository */
static void print_help(char *args){
	u16 i = 0;
	u16 no_of_commands = sizeof(cmd_table)/sizeof(cli_command_definition_t);
	for(i = 0; i < no_of_commands; i++){
		print((char*)cmd_table[i].command_name);
		print(" \t- ");

		print((char*)cmd_table[i].help_string);
		print("\n");
	}
}

/* The following function is from the NUTS software repository, and modified */
void run(char *command) {
	char *command_name, *command_parameters;
	u8 command_length, param_length;
	u16 i = 0;

	u16 no_of_commands = sizeof(cmd_table)/sizeof(cli_command_definition_t);

	command_name = get_parameter(command, 0, &command_length);
	command_parameters = get_parameter(command, 1, &param_length);

	bool match_found = false;
	for(i = 0; i < no_of_commands; i++){
		if(!strncmp(command_name, cmd_table[i].command_name, command_length)){
			cmd_table[i].command(command_parameters);
			match_found = true;
		}
	}
	if(!match_found){
		print("Unknown command. Type help for a list of commands.\n");
	}
}

enum escape_state {WaitEscape, WaitBracket, WaitChar}; // states used to parse ANSI escape codes for special keys

void cli_loop() {
	history_reset(&history);

	u8 c;

	enum escape_state state;
	state = WaitEscape;

	while (1) {
		// Format is "01> user_entered_command_here". ESC[J clears line, ESC[nG sets cursor position
		ee_printf("\r\x1b[J%2u> %s\x1b[%iG", history.current, history.current_line->line, history.current_line->cursor + 5);

		c = XUartLite_RecvByte(STDIN_BASEADDRESS);

		switch (state) { // handle stateful keys before anything else
			case WaitEscape:
				if (c == 0x1b) { // escape key
					state = WaitBracket;
					continue;
				}
				break;
			case WaitBracket:
				if (c == '[') {
					state = WaitChar;
				} else
					state = WaitEscape;
				continue;
			case WaitChar:
				state = WaitEscape;
				switch (c) {
					case 'A': history_move_cursor(&history, Up); continue;
					case 'B': history_move_cursor(&history, Down);	continue;
					case 'C': history_move_cursor(&history, Right); continue;
					case 'D': history_move_cursor(&history, Left);	continue;
					default:
						continue;
				}
				break;
			default:
				state = WaitEscape;
				break;
		}

		switch (c) { // handle non-escape codes
			case 0x7F: history_remove_key(&history, Right); break;// backspace
			case '\b': history_remove_key(&history, Left); break; // delete
			case '\r':
			case '\n':
				print("\n");
				if (history.current_line->length > 0) { // something has to be input
					run(history.current_line->line);
					history_move_cursor(&history, Down);
					if (c == '\r')
						line_buffer_reset(history.current_line); // clear to prepare for new command
				}
				break;
			case 0x03: // Ctrl-C
			case 0x15: // Ctrl-U
				line_buffer_reset(history.current_line);
				break;
			default:
				history_insert_key(&history, c);
				break;
		}
	}
}
