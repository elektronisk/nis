#include <stdlib.h>
#include "xparameters.h"
#include "gpio.h"
#include "xiic.h"
#include "../cli.h"
#include "../../ee_printf.h"
extern XIic iic_instance;

/* Toggle IOs. Argument is the byte to set the IO bits to */
void gpio(char *args) {
	u8 value, len = 0;
	char *value_argument = get_parameter(args, 0, &len);
	if (value_argument == NULL) {
		print("Usage: gpio <bytevalue>\n");
		return;
	}
	value = strtoul(value_argument, NULL, 0);
	XIic_SetGpOutput(&iic_instance, value);
	ee_printf("GPIO set to: 0x%02x\n", value);
}
