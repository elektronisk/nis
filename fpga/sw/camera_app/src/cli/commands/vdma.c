#include "xaxivdma.h"
#include "vdma.h"
#include "../../ee_printf.h"

XAxiVdma vdma_instance;
XAxiVdma_DmaSetup vdma_setup;
XAxiVdma_Config *config;

u16 frame[RES_Y][STRIDE];

/* Provide control of VDMA module used to capture frames to memory */

void vdma_callback(void *ref, u32 pending) {
	print("In VDMA GENERAL CALLBACK\n");
}

void vdma_callback_error(void *ref, u32 pending) {
	print("In VDMA ERROR CALLBACK\n");
}

void vdma(char *args) {
	config = XAxiVdma_LookupConfig(0);
	if (config == NULL) {
		print("Could not find config\n");
		return;
	}
	XAxiVdma_Reset(&vdma_instance, XAXIVDMA_WRITE);
	print("Found device, configuring.. ");
	if (XAxiVdma_CfgInitialize(&vdma_instance, config, config->BaseAddress) != XST_SUCCESS) {
		print("Could not initialize VDMA\n");
		return;
	} else { print("OK\n");}

	print("Setting up callbacks.. ");
	if (XAxiVdma_SetCallBack(&vdma_instance, XAXIVDMA_HANDLER_GENERAL,vdma_callback,
0,XAXIVDMA_WRITE) != XST_SUCCESS) {
		print("Error");
		return;
	} else { print("OK\n"); }

	// this may not be required:
	if (XAxiVdma_SetFrmStore(&vdma_instance, 1, XAXIVDMA_WRITE) != XST_SUCCESS) {
		print("Could not set frame store \n");
		return;
	}

	vdma_setup.HoriSizeInput = RES_X*2; // number of active video bytes in a scanline
	vdma_setup.VertSizeInput = RES_Y; // number of lines
	vdma_setup.FrameStoreStartAddr[0] = (u32)&frame;
	vdma_setup.Stride = STRIDE*2; // number of bytes per line
	vdma_setup.FrameDelay = 0;
	vdma_setup.EnableFrameCounter = 1; // Capture 1 frame. If 0, run continuously.
	vdma_setup.FixedFrameStoreAddr = 0;
	vdma_setup.EnableCircularBuf = 0;

	print("StartWriteFrame.. ");

	// must select tuser framesync, since this is the one provided by video in to axi4 stream module.
	if (XAxiVdma_FsyncSrcSelect(&vdma_instance, XAXIVDMA_S2MM_TUSER_FSYNC, XAXIVDMA_WRITE) != XST_SUCCESS) {
		print("Error: Could not select tuser fsync source.\n");
		return;
	}

	switch(XAxiVdma_StartWriteFrame(&vdma_instance, &vdma_setup)) {
		case XST_SUCCESS: print("OK!\n"); break;
		case XST_DEVICE_BUSY: print("Device busy\n"); return; break;
		case XST_INVALID_PARAM: print("Invalid param\n"); return; break;
		case XST_DEVICE_NOT_FOUND: print("Dev not found\n"); return; break;
	}

	while(XAxiVdma_IsBusy(&vdma_instance,XAXIVDMA_WRITE)){};

	ee_printf("Frame address: %p\n", &frame);
	return;
}
