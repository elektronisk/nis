#include <stddef.h>
#include <ctype.h>
#include <stdlib.h>
#include "img.h"
#include "xiic.h"
#include "xparameters.h"
#include "../cli.h"
#include "../../ee_printf.h"

#define IIC_BASE_ADDRESS XPAR_AXI_IIC_0_BASEADDR
#define SENSOR_ADDRESS 0x5d // 0xBA - write mode, 0xBB read mode

const static char message[] = "Usage: img <r|w> <address> [value|size] (register operation)\n";

struct registerEntry registerEntries[] = {
	{0, "Chip Version"}, {1, "Row Start"}, {2, "Column Start"}, {3, "Row Size"}, {4, "Column Size"},
	{5, "Horizontal Blank"}, {6, "Vertical Blank"}, {7, "Output Control"}, {8, "Shutter Width Upper"},
	{9, "Shutter Width Lower"}, {10, "Pixel Clock Control"}, {11, "Restart"}, {12, "Shutter Delay"},
	{13, "Reset"}, {16, "PLL Control"}, {17,"PLL Config 1"}, {18, "PLL Config 2"},
	{30, "Read Mode 1"}, {32, "Read Mode 2"}, {34, "Row Address Mode"}, {35, "Column address mode"},
	{43, "Green1 Gain"}, {44, "Blue Gain"}, {45, "Red Gain"}, {46, "Green2 Gain"},
	{53, "Global Gain"}, {75, "Row black offset"}, {160, "TestPatternControl"}, {161, "Test Pattern Green"},
	{162, "Test Pattern Red"}, {163, "Test Pattern Blue"}, {164, "TestPatBarWidth"}, {255, "Chip Version Alt"}
};

s16 find_register(struct registerEntry entries[], u8 index) { // find register starting at index
	u16 i = 0;
	for (i = 0; i < sizeof(registerEntries)/sizeof(struct registerEntry); i++) {
		if (entries[i].index == index) {
			return i;
		}
	}
	return -1;
}

int img_read_register(u8 address, u16 *value) {
	u8 buffer[2];
	if(XIic_Send(IIC_BASE_ADDRESS, SENSOR_ADDRESS, &address, 1, XIIC_STOP) != 1) {
		return XST_FAILURE;
	}
	if (XIic_Recv(IIC_BASE_ADDRESS, SENSOR_ADDRESS, buffer, 2, XIIC_STOP) != 2) {
		return XST_FAILURE;
	}
	*value = ((buffer[0] << 8) & 0xFF00) + buffer[1];
	return XST_SUCCESS;
}

int img_write_register(u8 address, u16 value) {
	u8 buffer[3] = {SENSOR_ADDRESS, 0, 0};
	buffer[0] = address;
	buffer[1] = (value & 0xFF00) >> 8;
	buffer[2] = (value & 0xFF);
	if (XIic_Send(IIC_BASE_ADDRESS, SENSOR_ADDRESS, buffer, 3, XIIC_STOP) != 3){
		return XST_FAILURE;
	} else {
		return XST_SUCCESS;
	}
}

void img_init() {
	int ret = 0;
	u16 register_value = 0;
	ret = img_write_register(IMG_RESET, 1); // software-reset of sensor
	ret |= img_write_register(IMG_RESET, 0);

	ret |= img_read_register(IMG_PIXEL_CLOCK_CONTROL, &register_value); //invert pixel clock
	register_value |= IMG_PIXEL_CLOCK_INVERT;
	ret |= img_write_register(IMG_PIXEL_CLOCK_CONTROL, register_value);

	ret |= img_write_register(IMG_OUTPUT_CONTROL, 0x1202); // set data output slew rate to 4 (half strength)
	ret |= img_write_register(IMG_GLOBAL_GAIN,16); // Sets the analog gain to 2
	if (ret != XST_SUCCESS)	print("Could not initialize\n");
	else print("Image sensor initialized\n");
}

void img(char *args) {
	u8 len, operation;
	char *operation_argument = get_parameter(args, 0, &len);
	char *address_argument = get_parameter(args, 1, &len);
	char *value_argument = get_parameter(args, 2, &len);

	if (operation_argument == NULL) {
		print((char*)message);
		return;
	}
	operation = tolower((int)*operation_argument);

	if (address_argument == NULL && (operation == 'r' || operation == 'w' || operation == 't')) {
		print((char*)message);
		return;
	}

	u16 value = 1;
	if (NULL != value_argument) {
		value = strtoul(value_argument, NULL, 0);
	}

	if (NULL == value_argument && tolower((int)*operation_argument) == 'w') {
		print((char*)message);
		return;
	}
	u8 address = (u8)strtoul(address_argument, NULL, 0);
	s16 tableindex = -1;
	u32 i;
	u16 content;

	switch(operation) {
		case 'r':
			tableindex = find_register(registerEntries, address);
			if (tableindex == -1) {
				print("Register not found, probably reserved");
				return;
			}
			if (XIic_Send(IIC_BASE_ADDRESS, SENSOR_ADDRESS, &address, 1, XIIC_STOP) != 1) {
				print("Error: could not send address\n");
				//return;
			}
			for (i = tableindex; (i < tableindex+value) && (i < sizeof(registerEntries)/sizeof(struct registerEntry)); i++) {
				if (img_read_register(registerEntries[i].index, &content) != XST_SUCCESS) {
					print("Read failed\n");
					return;
				}
				ee_printf("R%i: %d (0x%04x) %s\n", registerEntries[i].index, content, content ,  registerEntries[i].name );
			}
			break;
		case 'w':
			if (img_write_register(address, value) != XST_SUCCESS) {
				print("Write failed\n");
				return;
			}
			break;
		case 'i':
			img_init();
			break;
		case 't': // test command for test patterns
			// disable BLC
			img_write_register(32, 0); // clears row BLC bit
			img_write_register(75, 0); // default BLC offset, clear
			img_write_register(160, ((address & 0x0F) << 3) | 1 ); // set test pattern as given
			break;
		default:
			print((char*)message);
			break;
	}
	print("\n");

}
