#ifndef VDMA_H_
#define VDMA_H_

void vdma(char *args);

#define RES_X 2592
#define RES_Y 1944
#define STRIDE RES_X // Stride could be larger than x resolution

extern u16 frame[RES_Y][STRIDE]; // stride (x) is continuous in memory

#endif /* VDMA_H_ */
