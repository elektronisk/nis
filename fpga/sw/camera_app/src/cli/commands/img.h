#ifndef IIC_H_
#define IIC_H_

#include "xil_types.h"
void img_init();
void img(char* args);

struct registerEntry {
	u8 index;
	char name[20];
};

int img_write_register(u8 address, u16 value);
int img_read_register(u8 address, u16 *value);

#define IMG_OUTPUT_CONTROL 7
#define IMG_GLOBAL_GAIN 0x35

#define IMG_PIXEL_CLOCK_CONTROL	10
#define  IMG_PIXEL_CLOCK_INVERT 0x8000
#define IMG_RESET 13

#endif /* IIC_H_ */
