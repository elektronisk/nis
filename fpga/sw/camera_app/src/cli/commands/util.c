#include "util.h"
#include "xil_cache.h"
#include <stdlib.h>
#include <ctype.h>
#include "../cli.h"
#include "../../ee_printf.h"
#include "xuartlite_l.h"

/* Prints keycode values for keys pressed */
void util_printkey(char *args) {
	ee_printf("Press Ctrl-C (0x03) to exit\n");
	u8 c;
	while(1) {
		c = XUartLite_RecvByte(STDIN_BASEADDRESS);
		ee_printf("Hex: %02x Dec: %3u\n", c, c);
		if (c == 0x03) {
			return;
		}
	}
}

void util_reboot(char *args) {
	print("Rebooting..\n");
	void (*reset_vector)() = (void (*)())0;
	Xil_ICacheInvalidate();
	(*reset_vector)();
}
