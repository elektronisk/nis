#include <string.h>
#include "xil_types.h"
#include "dump.h"
#include "xuartlite_l.h"
#include "vdma.h"
#include "xparameters.h"
#include "../cli.h"
#include "../../ee_printf.h"

/* u16 frame[y][x] layout:
 *  x| 0  1    2  3    4...
 * y--------------------
 * 0 | G0 R  | G0 R  | G0 ...
 * 1 | B  G1 | B  G1 | B  ...
 *  --------------------
 * 2 | G0 R  | G0 R  | G0 ...
 * 3 | B  G1 | B  G1 | B  ...
 *  --------------------
 * 4 | G0 R  | G0 R  | G0 ...
 * .
 *
 * We would like to be able to throw away pixels, but would like to keep color information.
 * Therefore the grouping of 2x2 shown above should be kept, and groups should be discarded,
 * not individual pixels.
 */
#define BITDEPTH 12 // of the source data
#define MAX_VALUE (1 << BITDEPTH)-1
#define BIN_BITS 5
#define BIN_COUNT 1 << BIN_BITS
#define CONTRAST_ESTIMATE 32

u32 bins[BIN_COUNT];

void compute_histogram() {
	register u16 maxval = 0, minval = MAX_VALUE;
	register u16 mincount = 0, maxcount = 0;
	register u16 pixelvalue;
	u16 x, y, i;
	memset(bins, 0, sizeof(bins));

	for (y=0; y < RES_Y; y+=2) { // go through G0 component
		for (x = 0; x < STRIDE; x+=2) {
			pixelvalue = frame[y][x];
			if (pixelvalue == 0) mincount++;
			if (pixelvalue == MAX_VALUE) maxcount++;
			if (pixelvalue > maxval) maxval = pixelvalue;
			if (pixelvalue < minval) minval = pixelvalue;
			u16 bin_index = pixelvalue >> (BITDEPTH-BIN_BITS);
			bins[bin_index]++;
		}
	}

	for (i = 0; i < BIN_COUNT; i++) {
		ee_printf("Bin %02i: %o\n", i, bins[i]);
	}
	ee_printf("Minval: %i Maxval: %i\n", minval, maxval);
	ee_printf("Minfreq: %i Maxfreq: %i\n", mincount, maxcount);
}


int abs(int value){
	if(value < 0){
		return -value;
	} else {
		return value;
	}
}

int compute_brightness() {
	vdma(0);
	register u16 maxval = 0;//, minval = MAX_VALUE;
	register u16 pixelvalue;
	u16 x, y;
	for (y=0; y < RES_Y; y+=2) { // go through G0 component
		for (x = 0; x < STRIDE; x+=2) {
			pixelvalue = frame[y][x];
			if (pixelvalue > maxval) maxval = pixelvalue;
			//if (pixelvalue < minval) minval = pixelvalue;
		}
	}
	ee_printf("Brightness: %d\n",maxval);
	return maxval;
}

void compute_contrast() {
	vdma(0);
	u16 p=CONTRAST_ESTIMATE;
	register int contrast = 0;
	u16 x, y;
	// 32x32 pixel matrix from the middle of the image.
	for (y=(RES_Y/2-p); y < (RES_Y/2+p); y+=2) {
		for (x=(STRIDE/2-p); x < (STRIDE/2+p)-2; x+=2) {
			contrast += abs(frame[y][x]-frame[y][x+1]);
			//contrast += abs(frame[y][x]-frame[y-1][x]);
		}
	}
	ee_printf("Contrast: %i\n",contrast);
}

void dump(char *args) {
	u16 x,y;
	u8 skip = 8;
	u8 len;
	char *skip_argument = get_parameter(args, 0, &len);
	if (skip_argument == NULL) {
		print("Usage: dump <0-6|h|c>\n");
		return;
	}

	switch(skip_argument[0]) {
		// resolution evenly divides into 2, 3, 4, 6, 8, 12.
		// only 2, 3, 4, 6 can be deinterpolated immediately (even x,y resolutions)
		case '0': skip = 1; break;
		case '1': skip = 2; break;
		case '2': skip = 3; break;
		case '3': skip = 4; break;
		case '4': skip = 6; break;
		case '5': skip = 8; break;
		case '6': skip = 12; break;
		case 'h': compute_histogram(); return;
		case 'c': compute_contrast(); return;
		case 'b': compute_brightness(); return;
		default:
			print("Unknown argument size!\n");
			return;
	}

	ee_printf("####%p:%06x:%x****", (u8*)frame, (STRIDE * RES_Y * 2) / (skip*skip), skip);

	for (y=0; y < RES_Y; y += 2*skip) {
		for (x = 0; x < STRIDE; x += 2*skip) {
			// send green 0
			XUartLite_SendByte(STDOUT_BASEADDRESS, frame[y][x] & 0x00FF); // lsb first
			XUartLite_SendByte(STDOUT_BASEADDRESS, (frame[y][x] & 0xFF00) >> 8);
			// send red
			XUartLite_SendByte(STDOUT_BASEADDRESS, frame[y][x+1] & 0x00FF); // lsb first
			XUartLite_SendByte(STDOUT_BASEADDRESS, (frame[y][x+1] & 0xFF00) >> 8);
		}
		for (x = 0; x < STRIDE; x += 2*skip) {
			// send blue
			XUartLite_SendByte(STDOUT_BASEADDRESS, frame[y+1][x] & 0x00FF); // lsb first
			XUartLite_SendByte(STDOUT_BASEADDRESS, (frame[y+1][x] & 0xFF00) >> 8);
			// send green1
			XUartLite_SendByte(STDOUT_BASEADDRESS, frame[y+1][x+1] & 0x00FF); // lsb first
			XUartLite_SendByte(STDOUT_BASEADDRESS, (frame[y+1][x+1] & 0xFF00) >> 8);
		}
	}
	print("\n");
}
