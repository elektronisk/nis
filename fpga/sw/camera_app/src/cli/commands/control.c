/*
 * control.c
 *
 *  Created on: 15. des. 2014
 *      Author: Thomas
 */

#include "control.h"
#include "dump.h"
#include "img.h"
#include "../../ee_printf.h"
#include "vdma.h"
#include <stddef.h>
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>

const char *ctrlmessage = "Usage: ctrl <s/r> [value|size]\n";

float max(float value1,float value2){
	if(value1 > value2)
		return value1;
	else
		return value2;
}

float min(float v1,float v2){
	if(v1>v2)
		return v2;
	else
		return v1;
}

void set_slew_rate(u16 rate){
	if(rate > 7 || rate < 0){
		return;
	}
	u16 original,mask;
	mask = 0b111;
	mask = mask << 10;
	rate = rate << 10;
	img_read_register(0x007,&original);
	ee_printf("Output Control: %#08x\n",original);
	original = (original & ~mask) | rate;
	img_write_register(0x007, original);
	img_read_register(0x007,&rate);
	ee_printf("The slewrate is now %#08x\n",rate);
}

void set_shutter_time(int denominator){
	ee_printf("Shutter: 1/%d\n",denominator);

	u16 sw,swl=0,swu=0; // Shutter Width (Upper/Lower)
	u16 sd,sdmax; // Shutter Delay
	u16 so,rowbin; // Shutter Overhead
	float texp;
	int w=RES_X;
	int v=RES_Y;
	int err=0;
	float limit;
	float temp;

	u16 hb=1,hbmin=450;
	u16 vb,vbmin;

	float trow;
	float pixclk=48000000.00; // Default pixclk

	err = img_read_register(0x008,&swu);
	err = img_read_register(0x009,&swl);
	ee_printf("swu: %d, swl: %d\n",swu,swl);

	sw = max(1,(2*16*swu)+swl);

	if(sw < 3)
		sdmax=1504;
	else
		sdmax=1232;
	ee_printf("sw: %d\n",sw);

	err = img_read_register(0x006,&vb);
	vb = vb + 1;
	ee_printf("vb: %d\n",vb);

	err = img_read_register(0x005,&hb);
	hb = hb + 1;
	ee_printf("hb: %d\n",hb);

	err = img_read_register(0x00C,&sd);
	sd=sd+1;
	ee_printf("sd: %d\n",sd);

	err = img_read_register(0x022,&rowbin);
	//rowbin=0; // Will always be zero for our use
	ee_printf("rowbin: %d\n",rowbin);
	so=208*(rowbin+1)+98+min(sd,sdmax)-94;
	ee_printf("so: %d\n",so);

	// Finding the value of trow
	pixclk=1/pixclk;
	trow=2*pixclk*max(((w/2)+max(hb,hbmin)),(41+346*(rowbin+1)+99));
	ee_printf("trow: %f\n",trow);
	//tframe=h+max(vb,vbmin)*trow;

	limit = sw*trow;
	texp=limit-so*2*pixclk;
	ee_printf("texp: %f\n",texp);

	// Regner ut ny verdi
	texp= 1/(float)denominator;
	//if(texp <= limit){
		ee_printf("New texp: %f\n",texp);
		temp = (texp+so*2*pixclk)/trow; // Finding sw
		ee_printf("TEMP: %f\n",temp);
		temp = temp-2*16*swu; // finding swu
		swl=(u16)temp;
		ee_printf("swl: %d\n",swl);
		img_write_register(0x009, swl);
	//} else {
	//	ee_printf("Choose a shorter shutter time.\n");
	//}
}

void control(char* args){
	u8 len, operation;
	char *operation_argument = get_parameter(args, 0, &len);
	char *value_argument = get_parameter(args, 1, &len);

	if (operation_argument == NULL || value_argument == NULL) {
		print((char*)ctrlmessage);
		return;
	}
	operation = tolower((int)*operation_argument);

	int value = 1;
	if (NULL != value_argument) {
		value = strtoul(value_argument, NULL, 0);
	}

	switch(operation){
		case 's':
			set_shutter_time(value);
			break;
		case 'r':
			set_slew_rate(value);
			break;
		default:
			print((char*)ctrlmessage);
			break;
	}


	//int a = 0;
	/*img_write_register(0x02B, 1);
	img_write_register(0x02C, 1);
	img_write_register(0x02D, 1);
	img_write_register(0x02E, 1);*/
	//img_write_register(0x035, 0x0008);

	//a = compute_brightness();
	//ee_printf("%d\n",a);
	//set_shutter_time(0);
}
