#include <stddef.h>
#include <ctype.h>
#include <stdlib.h>
#include "mem.h"
#include "xil_types.h"
#include "xuartlite_l.h"
#include "xparameters.h"
#include "../cli.h"
#include "../../ee_printf.h"

#define BYTES_PER_LINE 16

/* Provide memory inspection and modification ability from the command line */

const char *message = "Usage: mem <r|w|d> <b|h|w> <hexaddress> [value|size]\n";

u32 *align_address(u32 *address, u8 datasize) {
	switch(datasize) {
		case 1: return address;
		case 2: return (u32*)((u32)address & 0xFFFFFFFE); // mask lowest bit
		case 4: return (u32*)((u32)address & 0xFFFFFFFC); // mask two lowest bits
		default: return 0;
	}
}
u32 get_mem(u32 *address, u8 datasize, u32 index) {
	u32 output;
	switch(datasize) {
		case 1: output = 0x000000FF & ((char*)address)[index]; break;
		case 2: output = 0x0000FFFF & ((u16*)address)[index]; break;
		case 4: output = ((u32*)address)[index]; break;
	}
	return output;
}

void put_mem(u32 *address, u8 datasize, u32 index, u32 value) {
	switch(datasize) {
		case 1: ((char*)address)[index] = 0x000000FF & value; break;
		case 2: ((u16*)address)[index] = 0x0000FFFF & value; break;
		case 4: ((u32*)address)[index] = value; break;
	}
}

void util_mem(char *args) {
	microblaze_invalidate_dcache();
	u8 len;
	char *operation_argument = get_parameter(args, 0, &len);
	char *datasize_argument = get_parameter(args, 1, &len);
	char *address_argument = get_parameter(args, 2, &len);
	char *value_argument = get_parameter(args, 3, &len);

	if (NULL == operation_argument || NULL == address_argument || NULL == datasize_argument) {
		print((char*)message);
		return;
	}
	u32 value = 1;
	u8 datasize = 4;
	// a value is required, except for reads where a length of 1 is implied
	if (NULL != value_argument) {
		value = strtoul(value_argument, NULL, 0);
	}

	if (NULL == value_argument && tolower((int)*operation_argument) != 'r') {
		print((char*)message); // writes require a value argument
		return;
	}
	u32 i;
	u32 *address = (u32*)strtoul(address_argument, NULL, 16);

	switch(tolower((int)*datasize_argument)) {
		case 'b': datasize = 1; break;
		case 'h': datasize = 2; break;
		case 'w': datasize = 4; break;
		default: print((char*)message); return;
	}

	switch(tolower((int)*operation_argument)) {
		case 'r':
			address = align_address(address, datasize);

			print("\t \x1b[1m");
			for (i = 0; (i < BYTES_PER_LINE/datasize) && i < value; i++) {
				ee_printf("%*x", datasize*2 + 1, i*datasize); //print address header
			}
			print("\x1b[0m");
			for (i = 0; i < value; i++) { // print actual values read
				if ((i*datasize) % BYTES_PER_LINE == 0) {
					ee_printf("\n%p: ", (u32*)((u32)address + i*datasize));
				}
				ee_printf("%0*p ", datasize*2, get_mem((u32*)address, datasize, i));
			}
			break;
		case 'w':
			if (address != align_address(address, datasize)) {
				print("Unaligned address\n");
				return;
			}
			put_mem(address, datasize, 0, value); //perform write and print result
			ee_printf("%p: %0*p", address, datasize*2, get_mem(address, datasize, 0));
			break;
		default:
			print((char*)message);
			break;
	}
	print("\n");
}
