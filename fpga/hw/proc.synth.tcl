proc pnsynth {} {
  cd C:/Users/andreas/Documents/nis/fpga/hw/proc
  if { [ catch { xload xmp proc.xmp } result ] } {
    exit 10
  }
  if { [catch {run netlist} result] } {
    return -1
  }
  return $result
}
if { [catch {pnsynth} result] } {
  exit -1
}
exit $result
