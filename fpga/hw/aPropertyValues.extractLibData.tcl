cd .
if { [ catch { xload xmp aPropertyValues.xmp } result ] } {
  exit 10
}
set sim_x_lib [xget sim_x_lib]
set filename "aPropertyValues_lib_info.dat"
if { [catch {open $filename w } fh] } {
 puts "ERROR: unable to open $filename: $fh"
 exit -1
}
puts $fh "sim_x_lib=$sim_x_lib"
close $fh
exit 0
