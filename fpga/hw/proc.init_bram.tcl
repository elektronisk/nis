cd C:/Users/andreas/Documents/nis/fpga/hw/proc
if { [ catch { xload xmp proc.xmp } result ] } {
  exit 10
}

if { [catch {run init_bram} result] } {
  exit -1
}

exit 0
