LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

library UNISIM;
use UNISIM.Vcomponents.ALL;

ENTITY camera_top_tb IS
END camera_top_tb;

ARCHITECTURE behavior OF camera_top_tb IS
	--Inputs
	signal clk_100mhz  : std_logic                     := '0';
	signal data        : std_logic_vector(11 downto 0) := (others => '0');
	signal frame_valid : std_logic                     := '0';
	signal line_valid  : std_logic                     := '0';
	signal pixclk      : std_logic                     := '0';
	signal reset       : std_logic                     := '1';
	signal uart_in     : std_logic                     := '0';

	--BiDirs
	signal dram_rzq  : std_logic;
	signal dram_udqs : std_logic;
	signal dram_dqs  : std_logic;
	signal dram_dq   : std_logic_vector(15 downto 0);

	--Outputs
	signal extclk     : std_logic;
	signal uart_out   : std_logic;
	signal dram_we_n  : std_logic;
	signal dram_udm   : std_logic;
	signal dram_ras_n : std_logic;
	signal dram_ldm   : std_logic;
	signal dram_clk_n : std_logic;
	signal dram_clk   : std_logic;
	signal dram_cke   : std_logic;
	signal dram_cas_n : std_logic;
	signal dram_ba    : std_logic_vector(1 downto 0);
	signal dram_addr  : std_logic_vector(12 downto 0);

	-- Clock period definitions
	constant clk_100mhz_period : time := 10 ns;

	signal dram_dqs_vector : std_logic_vector(1 downto 0);
	signal dram_dm         : std_logic_vector(1 downto 0);
	signal trigger         : std_logic := '0';

	signal mcbx_command : std_logic_vector(2 downto 0);
	signal mcbx_enable1 : std_logic;
	signal mcbx_enable2 : std_logic;
	signal gpo :std_logic_vector(6 downto 0);
	signal scl, sda :std_logic;
BEGIN

	-- Instantiate the Unit Under Test (UUT)
	uut : entity work.camera_top PORT MAP(
			clk_100mhz  => clk_100mhz,
--			data        => data,
--			frame_valid => frame_valid,
--			line_valid  => line_valid,
--			pixclk      => pixclk,
--			extclk      => extclk,
			reset       => reset,
			uart_in     => uart_in,
			uart_out    => uart_out,
			dram_we_n   => dram_we_n,
			dram_udm    => dram_udm,
			dram_ras_n  => dram_ras_n,
			dram_ldm    => dram_ldm,
			dram_clk_n  => dram_clk_n,
			dram_clk    => dram_clk,
			dram_cke    => dram_cke,
			dram_cas_n  => dram_cas_n,
			dram_ba     => dram_ba,
			dram_addr   => dram_addr,
			dram_rzq    => dram_rzq,
			dram_udqs   => dram_udqs,
			dram_dqs    => dram_dqs,
			dram_dq     => dram_dq,
			iic_sda => sda,
			iic_scl => scl,
			gpo => gpo
		);

	rzq_pulldown1 : PULLDOWN port map(O => dram_rzq);
	dram_dm <= dram_udm & dram_ldm;

	--	dram_dqs_vector(1) <= dram_udqs when dram_dm(1) = '0' else 'Z';
	--	dram_dqs_vector(0) <= dram_dqs when dram_dm(0) = '0' else 'Z';
	--
	--	dram_dqs  <= 'Z' when dram_dm(0) = '0' else dram_dqs_vector(0);
	--	dram_udqs <= 'Z' when dram_dm(1) = '0' else dram_dqs_vector(1);

	mcbx_command <= (dram_ras_n & dram_cas_n & dram_we_n);

	process(dram_clk)
	begin
		if (rising_edge(dram_clk)) then
			if (reset = '1') then
				mcbx_enable1 <= '0';
				mcbx_enable2 <= '0';
			elsif (mcbx_command = "100") then
				mcbx_enable2 <= '0';
			elsif (mcbx_command = "101") then
				mcbx_enable2 <= '1';
			else
				mcbx_enable2 <= mcbx_enable2;
			end if;
			mcbx_enable1 <= mcbx_enable2;
		end if;
	end process;

	dram_dqs_vector <= (dram_udqs & dram_dqs) when (mcbx_enable2 = '0' and mcbx_enable1 = '0') else "ZZ";
	dram_dqs <= dram_dqs_vector(0) when (mcbx_enable1 = '1') else 'Z';
	dram_udqs <= dram_dqs_vector(1) when (mcbx_enable1 = '1') else 'Z';
	
	mem : entity work.mobile_ddr port map(
			Dq    => dram_dq,
			Dqs   => dram_dqs_vector,
			Addr  => dram_addr,
			Ba    => dram_ba,
			Clk   => dram_clk,
			Clk_n => dram_clk_n,
			Cke   => dram_cke,
			Cs_n  => '0',
			Ras_n => dram_ras_n,
			Cas_n => dram_cas_n,
			We_n  => dram_we_n,
			Dm    => dram_dm);

	sensor : entity work.mt9p031 (RTL) port map(
			OE          => '0',
			TRIGGER     => trigger,
			EXTCLK      => extclk,
			PIXCLK      => pixclk,
			RESET_BAR   => reset,
			DOUT        => data,
			FRAME_VALID => frame_valid,
			LINE_VALID  => line_valid);

	-- Clock process definitions
	clk_100mhz_process : process
	begin
		clk_100mhz <= '0';
		wait for clk_100mhz_period / 2;
		clk_100mhz <= '1';
		wait for clk_100mhz_period / 2;
	end process;

	-- Stimulus process
	stim_proc : process
	begin
		-- hold reset state for 100 ns.
		trigger <= '0';
		
		wait for 100 ns;
		reset <= '0';
		wait for 100 ns;
		trigger <= '1';
		-- insert stimulus here 

		wait;
	end process;

END;
