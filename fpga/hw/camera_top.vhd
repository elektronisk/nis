library ieee;
use ieee.std_logic_1164.ALL;
use ieee.numeric_std.ALL;
library UNISIM;
use UNISIM.Vcomponents.ALL;

entity camera_top is
	port(clk_100mhz  : in    std_logic;
		 data        : in    std_logic_vector(11 downto 0);
		 frame_valid : in    std_logic;
		 line_valid  : in    std_logic;
		 pixclk      : in    std_logic;
		 extclk      : out   std_logic;
		 nreset       : in    std_logic;
		 uart_in     : in    std_logic;
		 uart_out    : out   std_logic;
		 axi_spi_0_SCK_pin : inout std_logic;
		 axi_spi_0_MISO_pin : inout std_logic;
		 axi_spi_0_MOSI_pin : inout std_logic;
		 axi_spi_0_SS_pin : inout std_logic;
		 dram_we_n   : out   std_logic;
		 dram_udm    : out   std_logic;
		 dram_ras_n  : out   std_logic;
		 dram_ldm    : out   std_logic;
		 dram_clk_n  : out   std_logic;
		 dram_clk    : out   std_logic;
		 dram_cke    : out   std_logic;
		 dram_cas_n  : out   std_logic;
		 dram_ba     : out   std_logic_vector(1 downto 0);
		 dram_addr   : out   std_logic_vector(12 downto 0);
		 dram_rzq    : inout std_logic;
		 dram_udqs   : inout std_logic;
		 dram_dqs    : inout std_logic;
		 dram_dq     : inout std_logic_vector(15 downto 0);
		 iic_sda : inout std_logic;
		 iic_scl : inout std_logic;
		 gpo : out std_logic_vector(6 downto 0);
		 oe_bar : out std_logic);
end camera_top;

architecture BEHAVIORAL of camera_top is
	attribute BOX_TYPE : string;
	signal sensor_de     : std_logic;
	signal sensor_vblank : std_logic;
	signal sensor_hblank : std_logic;
	component proc
		port(RESET            : in    std_logic;
			 FT2232_UART_SIN  : in    std_logic;
			 sensor_data      : in    std_logic_vector(11 downto 0);
			axi_spi_0_SCK_pin : INOUT std_logic;
			axi_spi_0_MISO_pin : INOUT std_logic;
			axi_spi_0_MOSI_pin : INOUT std_logic;
			axi_spi_0_SS_pin : INOUT std_logic;      
			 mcbx_dram_we_n   : out   std_logic;
			 mcbx_dram_udm    : out   std_logic;
			 mcbx_dram_ras_n  : out   std_logic;
			 mcbx_dram_ldm    : out   std_logic;
			 mcbx_dram_clk_n  : out   std_logic;
			 mcbx_dram_clk    : out   std_logic;
			 mcbx_dram_cke    : out   std_logic;
			 mcbx_dram_cas_n  : out   std_logic;
			 FT2232_UART_SOUT : out   std_logic;
			 mcbx_dram_ba     : out   std_logic_vector(1 downto 0);
			 mcbx_dram_addr   : out   std_logic_vector(12 downto 0);
			 rzq              : inout std_logic;
			 mcbx_dram_udqs   : inout std_logic;
			 mcbx_dram_dqs    : inout std_logic;
			 mcbx_dram_dq     : inout std_logic_vector(15 downto 0);
			 sensor_hblank    : in    std_logic;
			 sensor_vblank    : in    std_logic;
			 sensor_de        : in    std_logic;
			 sensor_pixclk    : in    std_logic;
			 sensor_extclk    : out   std_logic;
			 CLK_100MHZ       : in    std_logic;
			 iic_sda : INOUT std_logic;
			 iic_scl : INOUT std_logic;
			 iic_gpo : OUT std_logic_vector(7 downto 0));
	end component;

	signal extclk_int     : std_logic;
	signal extclk_int_inv : std_logic;
	signal reset : std_logic;
	signal iic_gpo : std_logic_vector(7 downto 0);
begin
	proc_0 : proc
		port map(CLK_100MHZ               => clk_100mhz,
			     FT2232_UART_SIN          => uart_in,
			     RESET                    => reset,
			     sensor_extclk            => extclk_int,
			     sensor_pixclk            => pixclk,
			     sensor_data(11 downto 0) => data(11 downto 0),
			     sensor_de                => sensor_de,
			     sensor_hblank            => sensor_hblank,
			     sensor_vblank            => sensor_vblank,
			     FT2232_UART_SOUT         => uart_out,
					axi_spi_0_SCK_pin => axi_spi_0_SCK_pin,
					axi_spi_0_MISO_pin => axi_spi_0_MISO_pin,
					axi_spi_0_MOSI_pin => axi_spi_0_MOSI_pin,
					axi_spi_0_SS_pin => axi_spi_0_SS_pin,
			     mcbx_dram_addr           => dram_addr,
			     mcbx_dram_ba             => dram_ba,
			     mcbx_dram_cas_n          => dram_cas_n,
			     mcbx_dram_cke            => dram_cke,
			     mcbx_dram_clk            => dram_clk,
			     mcbx_dram_clk_n          => dram_clk_n,
			     mcbx_dram_ldm            => dram_ldm,
			     mcbx_dram_ras_n          => dram_ras_n,
			     mcbx_dram_udm            => dram_udm,
			     mcbx_dram_we_n           => dram_we_n,
			     mcbx_dram_dq             => dram_dq,
			     mcbx_dram_dqs            => dram_dqs,
			     mcbx_dram_udqs           => dram_udqs,
			     rzq                      => dram_rzq,
				  iic_sda						=> iic_sda,
				  iic_scl						=> iic_scl,
				  iic_gpo						=> iic_gpo);
	
	oe_bar <= '0';
	reset <= not nreset;
	gpo <= not iic_gpo(6 downto 0); -- bring rest of gpo out to the pins
	-- Translate from the sensors frame_valid and line_valid signals, to the
	-- _de and _vblank signals used by the video in to axi4 stream.
	-- The hblank is not used, but is passed so that it will be available
	-- for chipscope later
	sensor_vblank <= not frame_valid;
	sensor_hblank <= not line_valid;
	sensor_de     <= line_valid and frame_valid;

	-- Perform clock forwarding. The CLKOUT2 on the clock circuit is to be used to drive
	-- the image sensors clock input (called extclk here). In order to drive this, a regular
	-- OBUF cannot be used, so a ODDR2 block is used instead. It is gated using the I2C block
	-- general purpose outputs.
	extclk_int_inv <= NOT extclk_int;
	ODDR2_inst : ODDR2
		generic map(
			DDR_ALIGNMENT => "NONE",    -- Sets output alignment to "NONE", "C0", "C1" 
			INIT          => '0',       -- Sets initial state of the Q output to '0' or '1'
			SRTYPE        => "SYNC")    -- Specifies "SYNC" or "ASYNC" set/reset
		port map(
			Q  => extclk,               -- 1-bit output data
			C0 => extclk_int,           -- 1-bit clock input
			C1 => extclk_int_inv,       -- 1-bit clock input
			CE => iic_gpo(7),                  -- 1-bit clock enable input
			D0 => '0',                  -- 1-bit data input (associated with C0)
			D1 => iic_gpo(7),                  -- 1-bit data input (associated with C1)
			R  => '0',                  -- 1-bit reset input
			S  => '0'                   -- 1-bit set input
		);
end BEHAVIORAL;


