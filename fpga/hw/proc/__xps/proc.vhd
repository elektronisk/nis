LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY proc IS
PORT (
	rzq : INOUT STD_LOGIC;
	mcbx_dram_we_n : OUT STD_LOGIC;
	mcbx_dram_udqs : INOUT STD_LOGIC;
	mcbx_dram_udm : OUT STD_LOGIC;
	mcbx_dram_ras_n : OUT STD_LOGIC;
	mcbx_dram_ldm : OUT STD_LOGIC;
	mcbx_dram_dqs : INOUT STD_LOGIC;
	mcbx_dram_dq : INOUT STD_LOGIC_VECTOR(15 DOWNTO 0);
	mcbx_dram_clk_n : OUT STD_LOGIC;
	mcbx_dram_clk : OUT STD_LOGIC;
	mcbx_dram_cke : OUT STD_LOGIC;
	mcbx_dram_cas_n : OUT STD_LOGIC;
	mcbx_dram_ba : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
	mcbx_dram_addr : OUT STD_LOGIC_VECTOR(12 DOWNTO 0);
	RESET : IN STD_LOGIC;
	FT2232_UART_SOUT : OUT STD_LOGIC;
	FT2232_UART_SIN : IN STD_LOGIC;
	CLK_100MHZ : IN STD_LOGIC;
	sensor_pixclk : IN STD_LOGIC;
	sensor_de : IN STD_LOGIC;
	sensor_vblank : IN STD_LOGIC;
	sensor_hblank : IN STD_LOGIC;
	sensor_data : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
	sensor_extclk : OUT STD_LOGIC;
	axi_spi_0_SCK_pin : INOUT STD_LOGIC;
	axi_spi_0_MISO_pin : INOUT STD_LOGIC;
	axi_spi_0_MOSI_pin : INOUT STD_LOGIC;
	axi_spi_0_SS_pin : INOUT STD_LOGIC;
	iic_gpo : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	iic_sda : INOUT STD_LOGIC;
	iic_scl : INOUT STD_LOGIC
	);
END proc;

ARCHITECTURE STRUCTURE OF proc IS

BEGIN
END ARCHITECTURE STRUCTURE;
