library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity camera_top is
  port (
    rzq : inout std_logic;
    mcbx_dram_we_n : out std_logic;
    mcbx_dram_udqs : inout std_logic;
    mcbx_dram_udm : out std_logic;
    mcbx_dram_ras_n : out std_logic;
    mcbx_dram_ldm : out std_logic;
    mcbx_dram_dqs : inout std_logic;
    mcbx_dram_dq : inout std_logic_vector(15 downto 0);
    mcbx_dram_clk_n : out std_logic;
    mcbx_dram_clk : out std_logic;
    mcbx_dram_cke : out std_logic;
    mcbx_dram_cas_n : out std_logic;
    mcbx_dram_ba : out std_logic_vector(1 downto 0);
    mcbx_dram_addr : out std_logic_vector(12 downto 0);
    RESET : in std_logic;
    FT2232_UART_SOUT : out std_logic;
    FT2232_UART_SIN : in std_logic;
    CLK_100MHZ : in std_logic;
    sensor_clk : in std_logic;
    sensor_de : in std_logic;
    sensor_vblank : in std_logic;
    sensor_hblank : in std_logic;
    sensor_data : in std_logic_vector(11 downto 0)
  );
end camera_top;

architecture STRUCTURE of camera_top is

  component proc is
    port (
      rzq : inout std_logic;
      mcbx_dram_we_n : out std_logic;
      mcbx_dram_udqs : inout std_logic;
      mcbx_dram_udm : out std_logic;
      mcbx_dram_ras_n : out std_logic;
      mcbx_dram_ldm : out std_logic;
      mcbx_dram_dqs : inout std_logic;
      mcbx_dram_dq : inout std_logic_vector(15 downto 0);
      mcbx_dram_clk_n : out std_logic;
      mcbx_dram_clk : out std_logic;
      mcbx_dram_cke : out std_logic;
      mcbx_dram_cas_n : out std_logic;
      mcbx_dram_ba : out std_logic_vector(1 downto 0);
      mcbx_dram_addr : out std_logic_vector(12 downto 0);
      RESET : in std_logic;
      FT2232_UART_SOUT : out std_logic;
      FT2232_UART_SIN : in std_logic;
      CLK_100MHZ : in std_logic;
      sensor_clk : in std_logic;
      sensor_de : in std_logic;
      sensor_vblank : in std_logic;
      sensor_hblank : in std_logic;
      sensor_data : in std_logic_vector(11 downto 0)
    );
  end component;

 attribute BOX_TYPE : STRING;
 attribute BOX_TYPE of proc : component is "user_black_box";

begin

  proc_i : proc
    port map (
      rzq => rzq,
      mcbx_dram_we_n => mcbx_dram_we_n,
      mcbx_dram_udqs => mcbx_dram_udqs,
      mcbx_dram_udm => mcbx_dram_udm,
      mcbx_dram_ras_n => mcbx_dram_ras_n,
      mcbx_dram_ldm => mcbx_dram_ldm,
      mcbx_dram_dqs => mcbx_dram_dqs,
      mcbx_dram_dq => mcbx_dram_dq,
      mcbx_dram_clk_n => mcbx_dram_clk_n,
      mcbx_dram_clk => mcbx_dram_clk,
      mcbx_dram_cke => mcbx_dram_cke,
      mcbx_dram_cas_n => mcbx_dram_cas_n,
      mcbx_dram_ba => mcbx_dram_ba,
      mcbx_dram_addr => mcbx_dram_addr,
      RESET => RESET,
      FT2232_UART_SOUT => FT2232_UART_SOUT,
      FT2232_UART_SIN => FT2232_UART_SIN,
      CLK_100MHZ => CLK_100MHZ,
      sensor_clk => sensor_clk,
      sensor_de => sensor_de,
      sensor_vblank => sensor_vblank,
      sensor_hblank => sensor_hblank,
      sensor_data => sensor_data
    );

end architecture STRUCTURE;

